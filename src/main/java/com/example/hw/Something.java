package com.example.hw;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Something {

    @Id
    @GeneratedValue
    private Long id;

    private String someValue;

    public Something(String someValue) {
        this.someValue = someValue;
    }
}
