package com.example.hw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;

@RestController
public class Controller {

    @Autowired
    private SthRepo sthRepo;

    @RequestMapping("/isok")
    public String hello() {
        String ip = null;
        try (final DatagramSocket socket = new DatagramSocket()) {
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            ip = socket.getLocalAddress().getHostAddress();
        } catch (UnknownHostException | SocketException e) {
            e.printStackTrace();
        }
        Something something = new Something(new Date().toString());
        sthRepo.save(something);
        return "OK!!!!!!!! " + System.getenv("NODE_NR") + " " + ip + " sth count =" + sthRepo.count();
    }

    @RequestMapping("/ok")
    public String hello2() {
        System.out.println("value " + HwApplication.node);
        String ip = null;
        try (final DatagramSocket socket = new DatagramSocket()) {
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            ip = socket.getLocalAddress().getHostAddress();
        } catch (UnknownHostException | SocketException e) {
            e.printStackTrace();
        }


        return "OK!!!!!!!! " + System.getenv("NODE_NR") + " " + ip;
    }

    @RequestMapping("/k")
    public String hello3() {
        return "k";
    }

    @RequestMapping("/")
    public String hello4() {
        return "kkkkkkkkkkkkkk";
    }
}
